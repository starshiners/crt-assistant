# This project has moved to codeberg and will be removed from Gitlab in the future.

## You can find it on Codeberg now: [https://codeberg.org/liliumstar/crtassistant](https://codeberg.org/liliumstar/crtassistant)

### To update your existing local clone, run these commands:

#### 1. `git remote set-url origin https://codeberg.org/liliumstar/crtassistant.git`

#### 2. `git checkout main`

#### 3. `git pull`


# CRT Assistant

Automatically create torrent files, descriptions, and screenshots for CRT.

## Description

The goal of CRT Assistant is to take most of the work out of uploading standard Movies and TV. All you need to do is copy-paste the generated description into the relevant fields in upload form, double-check it, and you're done!

CRT Assistant will take any number of arguments and attempt to automatically generate a compliant description for each based on provided or guessed metadata. Screenshots and a poster image are automatically uploaded and included in the description, and a torrent file is generated alongside it.

### Features

- Batching
- Supports Movies and TV category
- Parses metadata from filenames, mediainfo and TMDB
- Compliant descriptions which include everything you need:
    - Properly formatted title
    - Relevant tags based on metadata
    - Re-uploaded poster image
    - 6 full resolution screenshots with async uploads to several image hosts
    - Formatted MediaInfo dump

## Getting Started

### Requirements

- [Python 3.9+](https://www.python.org/)
- [ffmpeg](https://ffmpeg.org/) on your path
- [mediainfo](https://mediaarea.net/en/MediaInfo) library, on your path
    - On Windows, this is included with the binary installer
    - For posix systems, it may be need to be installed seperately
- [TMDB API key](https://developers.themoviedb.org/3/getting-started/introduction)
- [imgbb API key](https://api.imgbb.com/) - **Optional**
- [freeimage API key](https://freeimage.host/page/api) - **Optional**
- [oxipng](https://github.com/shssoichiro/oxipng) - **Optional**, on your path

#### Tested Configurations

- Windows 10, Python 3.10.4
- Debian 11 (WSL), Python 3.9.2

### Installation

The installation follows the normal procedure of cloning a repo, activating a venv, and installing from requirements.txt

***Note**: Substitute the `python` command for whichever is appropriate for your system, such as `py` or `python3`*

1. `git clone https://gitlab.com/starshiners/crtassistant.git`
2. `cd crtassistant`
3. `python -m venv venv`
4. Activate the venv
    - Windows: `.\venv\Scripts\activate.ps1`
    - posix (bash): `source ./venv/bin/activate`
5. `python -m pip install -r requirements.txt`

### Docker

As an alternative installation method, you can use Docker. To build the docker image run

1. First build the image `docker build -t crtassistant https://gitlab.com/starshiners/crtassistant.git`
2. Run it with `sudo docker run --rm -it     -v path_to_movies:/data -v torrent_watch_folder:/watch -v /volume1/apps/crtassistant/config:/app/config -v /volume1/apps/crtassistant/presentations:/app/presentations  crtassistant




### Configuration

There is an `example_config.yaml` inside `./config`. To use it, simply change the filename or create a copy called `config.yaml` in the same directory, then substitute in the appropriate values. In [yaml](https://yaml.org/), use lowercase true/false. Strings without special characters can be inserted as is, otherwise use double quotes, like "value".

Certain values can be omitted, such as the API keys for imgbb and freeimage if you don't intend to use them. In that case, simply leave them as the default.

The config file is structured as follows:

- options:
    - compression: bool - Enables lossless png compression with oxipng (default: false)
    - hash_threads: int - Number of threads to use when hashing (default: 8)
    - interactive: bool - Whether to use interactive mode by default or not (default: true)
    - image_host: str - Either catbox, imgbb, or freeimage (default: catbox)
    - tonemap_algorithm: str - One of these: mobius, clip, linear, gamma, reinhard, or hable (default: reinhard)
- ignore:
    - enabled: bool - Whether or not to use the below ignore list (default: false)
    - ignore_list: list\[str\] - File extensions to ignore, without the '.'
- notes:
    - enabled: bool - Whether or not to use provided message (default: false)
    - note_text: str - The text to put between the \[notes\] tags
- keys:
    - tmdb: str - Your TMDB API key
    - crt: str - Your crt announce key, the value found between the tracker url and `/announce` with no slashes
    - imgbb: str - Your imgbb API key
    - freeimage: str - Your freeimage API key
- dirs:
    - temp: str - The directory used for temporary processing. (default: ./.crt_temp)
        - **Caution:** Do not use a directory which already has files in it or that another application expects access to.
    - presentation: str - The directory used to store created .torrent and description files. Each presentation will have it's own subdirectory inside here. (default: ./presentations)
- injection:
    - enabled: bool - Whether or not to use client injection / watch folder
    - type: str - Either qbittorrent, rtorrent (not implemented), or watch
    - qbittorrent:
        - webui: str - URL of the enabled qbittorrent webui
        - user: str - The username you've setup to access webui
        - password: str - The password you've setup to access webui
        - ssl: bool - Whether or not to verify ssl certs, turn off if using a self-signed one
    - watch: str - Full patch to the watch folder in which to copy the torrent file. Note that you must escape Window's backslashes.
    - rtorrent: str (not implemented) - XML RPC path to rtorrent. Can vary depending on implementation.

**Warning:** Just in case this needs to be said, keep your API keys and announce key to yourself, do not share them with anyone.

## Usage

### Syntax

CRT Assistant has several command line options, which can viewed by running `python ./crt.py --help`
```
usage: crt.py [-h] [-nt] [-ns] [-np] [-tm] [-m MESSAGE] [-y | -i] paths [paths ...]

CRT Assistant: Automatically create torrent files, descriptions, and screenshots for CRT.

positional arguments:
  paths                 All the files/folders you wish to process. Any names with spaces must be quoted

options:
  -h, --help            show this help message and exit
  -nt, --no_torrent     Do not create a torrent file, but do everything else.
  -ns, --no_screenshot  Do not create or upload screenshots (primarily for dev/testing).
  -np, --no_poster      Do not attempt to fetch poster (primarily for dev/testing).
  -tm, --tonemap        Enable HDR -> SDR tonemapping on screenshots. The algorithm is set in config.yaml
  -m MESSAGE, --message MESSAGE
                        Message you would like to use in the [notes] section. Overrides the "notes" section in config
  -y, --yes             Override the interactive configuration option, disabling all interactivity except in the case that crtassistant cannot find a TMDB ID
  -i, --interactive     Override the interactive configuration option and require that matches be confirmed per task
```

### Basic Usage

`python ./crt.py path1 path2...`

You can provide any number of paths, whether they are files or folders, that you wish to process. If there are spaces, they must of course be quoted. If the path is a directory, MediaInfo and screenshots are taken from the first file, up to one subdirectory deep.

It is recommended that you run CRT Assistant in interactive mode the first few times, so that you can confirm it's automatic identification is working properly with your file/folder structures.

**Note:** Each thing you plan to process must have a valid TMDB entry.

### Output

You can follow along in the terminal to check the progress of your jobs

Inside the presentation folder specified in the config, you will find a subfolder which contains the .torrent and .txt description file.

## Support

Should you encounter any bugs, please consider opening an issue here on gitlab or in an appropriate forum thread. Include any relevant input and output, including the trace in case of an unhandled exception, but redact or censor any potentially sensistive data.

### Known Issues

CRT Assistant has several current limitations, which should be resolved in future updates

- No full disc support
- 4k image uploads may fail due to exceeding the imgbb size limit
- Execution will stop on most exceptions
- No detection of specials for series

## Roadmap

### Implemented
- [x] Support for catbox image host
- [x] HDR tonemapping on screenshots
- [x] Add support for custom message in \[notes\] section
- [x] PNG compression (multi-threaded via oxipng)
- [x] Support for freeimage host
- [x] Add direct injection to one or more torrent clients

### Planned

- [ ] Guess if one or more specials are present in TV folders
- [ ] Full disc and bdinfo support
- [ ] Better error handling, so jobs/batches can be partially completed
- [ ] Additional tags, such as source
- [ ] Fancy console output


### Maybe

- [ ] WOC support (user-provided title and description, no TMDB calls)

### Not Happening

- Support for creating descriptions and mediainfo when it is required to have multiple sets of screenshots and mediainfo

## Contributing

If you would like to contribute, contact me to be added as a developer.

## Author

liliumstar

## Acknowledgments

Screenshot code is adapted from [gg-bot](https://gitlab.com/NoobMaster669/gg-bot-upload-assistant/), thanks!

CRT Assistant uses data from TMDB, and is not endorsed or affiliated with it.

## License

CRT Assistant is licensed under the GNU General Public License v3. The full text can be found [here](https://www.gnu.org/licenses/gpl-3.0.txt) or in the LICENSE file.
