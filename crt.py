from pathlib import Path
import argparse

import src.utils.fileutils as fileutils
import src.utils.validatedeps as validatedeps
from src.modules.worker import Worker


def create_parser() -> argparse.ArgumentParser:
    """Creates and returns the argument parser with all our arguments

    Returns:
        argparse.ArgumentParser: The created argparse object
    """
    parser = argparse.ArgumentParser(
        description='CRT Assistant: Automatically create torrent files, descriptions, and screenshots for CRT.')

    parser.add_argument(
        '-nt',
        '--no_torrent',
        action='store_true',
        help='Do not create a torrent file, but do everything else.'
    )

    parser.add_argument(
        '-ns',
        '--no_screenshot',
        action='store_true',
        help='Do not create or upload screenshots (primarily for dev/testing).'
    )

    parser.add_argument(
        '-np',
        '--no_poster',
        action='store_true',
        help='Do not attempt to fetch poster (primarily for dev/testing).'
    )

    parser.add_argument(
        '-tm',
        '--tonemap',
        action='store_true',
        help='Force HDR -> SDR tonemapping on screenshots. The algorithm is set in config.yaml'
    )

    parser.add_argument(
        '-m',
        '--message',
        type=str,
        default='',
        help='Message you would like to use in the [notes] section. Overrides the "notes" section in config'
    )

    interactive_group = parser.add_mutually_exclusive_group()

    interactive_group.add_argument(
        '-y',
        '--yes',
        action='store_true',
        help='Override the interactive configuration option, disabling all interactivity except in the case that crtassistant cannot find a TMDB ID'
    )

    interactive_group.add_argument(
        '-i',
        '--interactive',
        action='store_true',
        help='Override the interactive configuration option and require that matches be confirmed per task'
    )

    parser.add_argument(
        'paths',
        nargs='+',
        type=str,
        help='All the files/folders you wish to process. Any names with spaces must be quoted'
    )

    return parser


def main():
    config = fileutils.parse_config(
        Path(__file__).parent / 'config' / 'config.yaml')
    parser = create_parser()
    args = parser.parse_args()
    fileutils.check_temp_folder(config['dirs']['temp'])
    fileutils.check_presentations_folder(config['dirs']['presentations'])
    validatedeps.check_depedencies()
    if args.yes:
        config['options']['interactive'] = False
    elif args.interactive:
        config['options']['interactive'] = True
    if args.message != '':
        config['notes']['enabled'] = True
        config['notes']['note_text'] = args.message
    if config['keys']['crt'].startswith('http'):
        print('It looks like you put the entire announce url as the crt key, which will not work')
        exit(1)
    worker = Worker(args.paths, config, args)
    worker.run()


if __name__ == '__main__':
    main()
