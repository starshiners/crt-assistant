FROM alpine:3.18

RUN \
  echo "**** install build packages ****" && \
  apk add --no-cache --virtual=build-dependencies \
  g++ \
  libxml2-dev \
    libxslt-dev \
  py3-pip

RUN \
  echo "**** install runtime packages ****" && \
  apk add --no-cache --upgrade \
  ffmpeg \
  mediainfo \
  python3 \
  oxipng

WORKDIR /app
COPY . .
RUN \
  echo "**** install pip prerequisite packages ****" && \
  pip3 install -r requirements.txt && \
  pip3 freeze > requirements.txt

VOLUME /app/presentations /app/config

ENTRYPOINT [ "python3", "crt.py"]