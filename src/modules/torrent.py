from pathlib import Path
from datetime import datetime
from typing import List

from torf import Torrent


def create_torrent(media: Path, out_dir: Path, announce_key: str, threads: int, cb, ignore_globs: List[str]) -> str:
    """Creates and writes out a torrent

    Args:
        media (Path): Path to media to hash
        out_dir (Path): The directory to write out to
        announce_key (str): CRT announce key to use
        threads (int): Number of threads to use when hashing
        cb (function): Callback function for progress bar
        ignore_globs (List[str]): List of globs to ignore when hashing torrent.

    Raises:
        RuntimeError: If there was some unspecified error when hashing the torrent

    Returns:
        str: The name of the created file
    """
    piece_size = get_piece_size(media)
    announce = f'https://signal.cathode-ray.tube/{announce_key}/announce'
    media_name = media.name
    out_file = out_dir / f'{media_name}.torrent'
    torrent = Torrent(path=media, name=media.name, trackers=announce, private=True,
                      source='CRT', created_by='crt-assistant', piece_size=piece_size,
                      creation_date=datetime.now(), exclude_globs=ignore_globs)
    success = torrent.generate(threads=threads, callback=cb, interval=1)
    if success:
        torrent.write(out_file)
    else:
        raise RuntimeError('There was an issue generating the torrent')

    return out_file.name


def get_piece_size(media: Path) -> int:
    """Gets the piece size to use based on size of data to be hashed. Based 
        on CRT wiki page.
    Args:
        media (Path): Path object representing what is to be hashed.

    Returns:
        int: Piece size in bytes.
    """
    if media.is_file():    
        size = media.stat().st_size
    elif media.is_dir():
        size = sum(file.stat().st_size for file in media.rglob('*'))
    else:
        raise FileNotFoundError(f'The file/folder you are attempting to process does not seem to exist\nPath: {str(media)}')
    if size < 1073741824:  # 1 GiB
        return 524288
    elif size < 4294967296:  # 4 GiB
        return 2097152
    elif size < 8589934592:  # 8 GiB
        return 4194304
    elif size < 17179869184:  # 16 GiB
        return 8388608
    else:  # > 16 GiB
        return 16777216


if __name__ == '__main__':
    pass
