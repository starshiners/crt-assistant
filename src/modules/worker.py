from typing import List

import src.utils.fileutils as fileutils
from src.modules.jobitem import JobItem


class Worker():
    def __init__(self, job_paths: List[str], config: dict, args) -> None:
        """Creates, holds, and runs individual JobItem units for batch processing

        Args:
            job_paths (List[str]): User-provided paths to process
            config (dict): Configuration from config.yaml, with some elements possible overriden by args
            args (dict): Parsed command line arguments
        """
        parsed_job_paths = fileutils.parse_file_args(job_paths)
        self._jobs = []
        for paths in parsed_job_paths:
            self._jobs.append(JobItem(
                config=config, torrent_creation_path=paths['torrent_path'], info_path=paths['info_path'], tonemap_enabled=args.tonemap, no_poster=args.no_poster))
        self._job_count = len(self._jobs)
        self._current_job = 1
        self._no_torrent = args.no_torrent
        self._no_screenshot = args.no_screenshot

    def run(self) -> None:
        """Loops over all job objects
        """
        for job in self._jobs:
            self._do_individual_job(job)
            self._current_job += 1
        if self._job_count > 1:
            print('All jobs complete!')

    def _do_individual_job(self, job: JobItem) -> None:
        """Calls relevant methods of JobItem to fully perform each task, with helpful
            messages for impatient users

        Args:
            job (JobItem): The individual job to be processed
        """
        print(
            f'Count: {self._current_job} of {self._job_count}\n'
            f'Interactive: {job.interactive}\n'
            f'Image Compression: {job.compression}\n'
            f'Client injection: {job.injection}\n'
            f'Beginning work on {"file" if job.is_single_file else "folder"}: {job.torrent_creation_path.name}'
        )
        print('Fetching mediainfo...')
        job.get_mediainfo()
        if not self._no_screenshot:
            print('Taking and uploading screenshots...')
            job.do_screenshots()
        print('Taking guess on contents... ')
        job.take_guess()
        print('Fetching info from TMDB...')
        job.get_tmdb_info()
        print('Fetching poster...')
        job.fetch_poster()
        print('Creating description text...')
        job.create_description()
        if not self._no_torrent:
            job.create_torrent()
            if job.injection:
                print('Injecting torrent into client...')
                job.injector.inject(job.created_torrent_path, job.torrent_creation_path.parent)
        print(f'Job {self._current_job} complete!')
        print(f'Output location: {job.presentation_dir.resolve()}')


if __name__ == '__main__':
    pass
