from progress.bar import IncrementalBar


class TorrentBar(IncrementalBar):
    def __init__(self, *args, **kwargs) -> None:
        """A simple incremental progress bar with percent complete
        """        
        super().__init__(*args, **kwargs)
        self.max = 100
        self.message = 'Hashing'
        self.suffix = '%(percent).0f%%'


if __name__ == '__main__':
    pass
