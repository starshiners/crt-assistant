from pathlib import Path
import asyncio
from typing import List
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor

import pyimgbox
import aiohttp
from aiofiles import os as aios
from ffmpy import FFmpeg


def _get_ss_timestamps(duration: float) -> List[str]:
    """Generate a list of 6 equally spaced timestamps within the first half of the video

    Args:
        duration (float): Total duration of the video in milliseconds

    Returns:
        List[str]: timestampts to feed into ffmpeg
    """
    first_time_stamp = int(int(duration) / 2) / 7
    list_of_ss_timestamps = []
    for num_screen in range(7):
        millis = round(first_time_stamp) * num_screen
        list_of_ss_timestamps.append(
            str(
                datetime.strptime(
                    "%d:%d:%d" % (
                        int((millis / (1000 * 60 * 60)) % 24),
                        int((millis / (1000 * 60)) % 60),
                        int((millis / 1000) % 60)
                    ),
                    '%H:%M:%S'
                ).time()
            )
        )
    return list_of_ss_timestamps


async def _upload_screen(session: aiohttp.ClientSession, url: str, file: Path, image_host: str) -> str:
    """async function to upload a single screenshot

    Args:
        session (aiohttp.ClientSession): Active aiohttp session from the wrapper function
        url (str): Url to upload to
        file (Path): Single image file to upload
        image_host (str): The image host to use, valid values are 'catbox' and 'imgbb'

    Raises:
        aiohttp.ClientConnectionError: If response status code is not 200

    Returns:
        str: url to full resolution image that was uploaded
    """
    if image_host == 'imgbb':
        async with session.post(url, data={'image': file.open('rb')}) as res:
            if res.status == 200:
                parsed = await res.json()
                return parsed['data']['url']
            else:
                raise aiohttp.ClientConnectionError(
                    f'Cannot upload to imgbb, perhaps it is down?\nStatus code: {res.status}')
    elif image_host == 'catbox':
        async with session.post(url, data={'reqtype': 'fileupload', 'fileToUpload': file.open('rb')}) as res:
            if res.status == 200:
                parsed = await res.text()
                return parsed
            else:
                raise aiohttp.ClientConnectionError(
                    f'Cannot upload to catbox, perhaps it is down?\nStatus code: {res.status}')
    elif image_host == 'freeimage':
        async with session.post(url, data={'source': file.open('rb')}) as res:
            if res.status == 200:
                parsed = await res.json()
                return parsed['image']['url']
            else:
                raise aiohttp.ClientConnectionError(
                    f'Cannot upload to freeimage, perhaps it is down?\nStatus code: {res.status}')
    elif image_host == 'ptpimg':
        base_url, ptpkey = url.split()
        headers = {'referer': 'https://ptpimg.me/index.php'}
        data = {'api_key': ptpkey, 'file-upload[0]': file.open('rb')}
        async with session.post(base_url, headers=headers, data=data) as res:
            if res.status == 200:
                parsed = await res.json()
                image_url = f'https://ptpimg.me/{parsed[0]["code"]}.{parsed[0]["ext"]}'
                return image_url
            else:
                raise aiohttp.ClientConnectionError(
                    f'Cannot upload to ptpimg, perhaps it is down?\nStatus code: {res.status}')
    else:
        raise ValueError(
            f'Image host can only be imgbb, catbox, freeimage, or ptpimg, you provided: {image_host}')


async def take_screens_and_upload(video_file: Path,
                                  temp_folder: Path,
                                  duration: float,
                                  image_host: str,
                                  compression: bool,
                                  imgbb_key: str = '',
                                  needs_deint: bool = False,
                                  tonemap_command: str = '',
                                  freeimage_key: str = '',
                                  ptpimg_key: str = '') -> List[str]:
    """Generates timestamps, take screenshots, and then uploads them all concurrently

    Args:
        video_file (Path): Video file to take screenshots of
        temp_folder (Path): Folder in which to store screens temporarily
        duration (float): The total duration of video_file in milliseconds
        image_host (str): The image host to use, valid values are 'catbox' and 'imgbb'
        compresssion (bool): Whether or not compress images with oxipng
        imgbb_key (str): imgbb api key (Default: '')
        tonemap_command (str): Valid option to pass ffmpeg for tonemapping, including the '-vf' (Default: '')
        freeimage_key (str): freeimage api key (Default: '')
        ptpimg_key (str): ptpimg api key (Default: '')

    Returns:
        List[str]: URLs linking to all full resolution images uploaded
    """
    screenshot_files = []
    if image_host == 'imgbb':
        upload_url = f'https://api.imgbb.com/1/upload?key={imgbb_key}'
    elif image_host == 'catbox':
        upload_url = 'https://catbox.moe/user/api.php'
    elif image_host == 'freeimage':
        upload_url = f'https://freeimage.host/api/1/upload?key={freeimage_key}'
    elif image_host == 'ptpimg':
        upload_url = f'https://ptpimg.me/upload.php {ptpimg_key}'
    elif image_host == 'imgbox':
        pass
    else:
        raise ValueError(
            f'Image host can only be imgbb, catbox, freeimage, imgbox, or ptpimg, you provided: {image_host}')

    outputs = "-vf "

    if needs_deint:
        outputs += "yadif,"

    outputs += (
        "\"scale=w='if(gt(sar, 1), trunc(ih*dar), iw)':h='if(lt(sar, 1), trunc(iw/dar), ih)',setsar=1/1,"
        f'{tonemap_command}" '
        '-frames:v 1 -q:v 10 -c:v png -pix_fmt rgba -update 1'
    )

    for ss_timestamp in _get_ss_timestamps(duration)[1:]:
        video_stem = video_file.stem
        ss_path = temp_folder / \
            f'{video_stem}_{ss_timestamp.replace(":", ".")}.png'
        FFmpeg(
            inputs={str(video_file): f'-loglevel panic -ss {ss_timestamp}'},
            outputs={str(ss_path): outputs}
        ).run()
        screenshot_files.append(ss_path)

    if compression:
        print('Running oxipng task...')
        compresser = await asyncio.create_subprocess_exec('oxipng', '-q', '-o', '4', *[str(x) for x in screenshot_files])
        compresser_return = await compresser.wait()
        if compresser_return != 0:
            raise RuntimeError(
                f'Error: oxipng exited with status code: {compresser_return}')

    if image_host == 'imgbox':
        screenshots_urls = []
        async with pyimgbox.Gallery() as gallery:
            async for submission in gallery.add(screenshot_files):
                if not submission['success']:
                    raise aiohttp.ClientConnectionError(
                        f'Cannot upload to imgbox, perhaps it is down?\nError: {submission["error"]}')
                else:
                    screenshots_urls.append(submission['image_url'])
    else:
        async with aiohttp.ClientSession() as session:
            screenshots_urls = await asyncio.gather(*[_upload_screen(session, upload_url, x, image_host) for x in screenshot_files])
        for ss in screenshot_files:
            await aios.unlink(ss)

    return screenshots_urls


async def download_and_upload_poster(poster_url: str,
                                     temp_folder: Path,
                                     image_host: str,
                                     imgbb_key: str = '',
                                     freeimage_key: str = '',
                                     ptpimg_key: str = '') -> str:
    """Downloads a single poster image and reuploads it

    Args:
        poster_url (str): Post image to grab
        temp_folder (Path): Temporary storage location
        image_host (str): The image host to use, valid values are 'catbox' and 'imgbb'
        imgbb_key (str): imgbb api key (Default: '')
        freeimage_key (str): freeimage api key (Default: '')

    Raises:
        aiohttp.ClientConnectionError: If response status is anything other than 200

    Returns:
        str: URL to the uploaded poster image
    """
    user_supplied_poster = False
    if poster_url is None:
        poster_url = await ainput('Error: Could not find a poster on TMDB. Please enter a direct url link of one to download, or press <enter> to skip:\n')
        if not poster_url:
            return 'None: you must upload one yourself and link it here.'
        user_supplied_poster = True
    if image_host == 'imgbb':
        upload_url = f'https://api.imgbb.com/1/upload?key={imgbb_key}'
    elif image_host == 'catbox':
        upload_url = 'https://catbox.moe/user/api.php'
    elif image_host == 'freeimage':
        upload_url = f'https://freeimage.host/api/1/upload?key={freeimage_key}'
    elif image_host == 'ptpimg':
        upload_url = f'https://ptpimg.me/upload.php {ptpimg_key}'
    elif image_host == 'imgbox':
        pass
    else:
        raise ValueError(
            f'Image host can only be imgbb, catbox, freeimage, imgbox, or ptpimg, you provided: {image_host}')

    async with aiohttp.ClientSession() as session:
        async with session.get(poster_url) as res:
            if user_supplied_poster:
                save_path = temp_folder / f'{poster_url.split("/")[-1]}'
            else:
                save_path = temp_folder / f'{poster_url.split("/")[-1]}.jpg'
            if res.status == 200:
                content = await res.read()
                with save_path.open('wb') as fo:
                    fo.write(content)
            else:
                if user_supplied_poster:
                    raise aiohttp.ClientConnectionError(
                        'Supplied poster link is not working or is invalid.')
                else:
                    raise aiohttp.ClientConnectionError(
                        f'Found poster from TMDB, but could not download it. Perhaps an API issue?')
        if image_host == 'imgbox':
            async with pyimgbox.Gallery() as gallery:
                submission = await gallery.upload(save_path)
                if not submission['success']:
                    raise aiohttp.ClientConnectionError(
                        f'Cannot upload to imgbox, perhaps it is down?\nError: {submission["error"]}')
                else:
                    uploaded_url = submission['image_url']
        else:
            uploaded_url = await _upload_screen(session, upload_url, save_path, image_host)
            await aios.unlink(save_path)

    return uploaded_url


async def ainput(prompt: str = '') -> str:
    """Async version of input.
        https://gist.github.com/delivrance/675a4295ce7dc70f0ce0b164fcdbd798

    Args:
        prompt (str, optional): The prompt to give the user. Defaults to ''.

    Returns:
        str: The user's input.
    """
    with ThreadPoolExecutor(1, 'ainput') as executor:
        return (await asyncio.get_event_loop().run_in_executor(executor, input, prompt)).rstrip()


if __name__ == '__main__':
    pass
