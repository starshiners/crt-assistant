
def create_presentation(job) -> str:
    """Creates the text description for site based on properties of the provided job item

    Args:
        job (JobItem): Instance of JobItem which holds all information needed to create the
            description

    Returns:
        str: Entire description string for later writing to file
    """
    if job.screenshot_urls:
        screenshot_url_string = '\n'.join(job.screenshot_urls)
    else:
        screenshot_url_string = '\n'.join(['none'] * 6)
    notes_string = f'[notes]\n{job.note_text}\n[/notes]\n\n' if job.note_text else ''
    indexers = [x for x in job.indexer_urls if x is not None]
    indexers.append('https://gitlab.com/starshiners/crtassistant')
    indexers = '\n'.join(indexers)

    stars = '*****'
    out_string = (
        f'{stars} TORRENT FILE {stars}\n'
        f'{job.created_torrent_path.stem}.torrent\n\n'
        f'{stars} CATEGORY {stars}\n'
        f'{"Movies" if job.upload_type == "movie" else "TV"}\n\n'
        f'{stars} TITLE {stars}\n'
        f'{job.crt_title}\n\n'
        f'{stars} TAGS {stars}\n'
        f'{" ".join(job.tags)}\n\n'
        f'{stars} COVER IMAGE {stars}\n'
        f'{job.uploaded_poster_url}\n\n'
        f'{stars} DESCRIPTION {stars}\n'
        f'[info]\n'
        f'{indexers}\n'
        f'[/info]\n\n'
        f'[plot]\n'
        f'{job.tmdb_description}\n'
        f'[/plot]\n\n'
        f'{notes_string}'
        f'[screens]\n'
        f'{screenshot_url_string}\n'
        f'[/screens]\n\n'
        f'[details]\n[mediainfo]\n'
        f'{job.mediainfo_string}\n'
        f'[/mediainfo]\n[/details]\n'
    )

    return out_string


if __name__ == '__main__':
    pass
