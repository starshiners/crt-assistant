import asyncio
from pathlib import Path
import sys
from typing import List, Union
import subprocess
import json

from guessit import guessit

from src.modules.tmdb import TMDB
from src.modules.mediainfo import VideoInfo
from src.modules.torrentbar import TorrentBar
from src.modules.injector import Injector
import src.utils.ffprobe as ffprobe
import src.modules.images as images
import src.utils.fileutils as fileutils
import src.utils.textutils as textutils
import src.modules.presentation as presentation
import src.modules.torrent as torrent


class JobItem():
    def __init__(self, config: dict, torrent_creation_path: Path, info_path: Path, tonemap_enabled: bool, no_poster: bool) -> None:
        """Primary class with all data derived from a given path. Contains all methods
            needed in order to create description and torrent

        Args:
            config (dict): configuration parsed from config.yaml
            torrent_creation_path (Path): The path specified by the user to operate on
            info_path (Path): The same as the torrent_creation_path if it was a single file,
                otherwise the first valid media file found
        """
        self._config = config
        self._torrent_creation_path = torrent_creation_path
        self._info_path = info_path
        self._tmdb_key = config['keys']['tmdb']
        self._crt_key = config['keys']['crt']
        self._imgbb_key = config['keys']['imgbb']
        self._freeimage_key = config['keys']['freeimage']
        self._ptpimg_key = config['keys']['ptpimg']
        self._temp_dir = config['dirs']['temp']
        self._presentation_dir = config['dirs']['presentations'] / \
            self.torrent_creation_path.name
        self._created_torrent_path = self._presentation_dir / \
            f'{self.torrent_creation_path.name}.torrent'
        self._interactive = config['options']['interactive']
        self._hashing_threads = config['options']['hash_threads']
        self._image_host = config['options']['image_host']
        self._compression = config['options']['compression']
        if tonemap_enabled or ffprobe.check_needs_tonemap(self.info_path):
            self._tonemap_command = config['options']['tonemap_command']
        else:
            self._tonemap_command = ''
        if ffprobe.check_needs_deint(self.info_path):
            self._needs_deint = True
        else:
            self._needs_deint = False
        self._note_text = config['notes']['note_text'] if config['notes']['enabled'] else None
        self._valid_types = ['tv', 'movie']
        self._ignore_enabled = config['ignore']['enabled']
        if self._ignore_enabled:
            self._ignore_globs = [f'*.{x}' for x in config['ignore']['ignore_list']]
        else:
            self._ignore_globs = []
        self._upload_type = None
        self._tmdb_id = 0
        self._seasons = []
        self._guessed_year = 0
        self._language = ''
        self._release_group = None
        self._edition = None
        self._crt_title = ''
        self._uploaded_poster_url = ''
        self._tags = []
        self._presentation = ''
        self._screenshot_urls = []
        self._tmdb_description = ''
        self._mediainfo_string = ''
        self._no_poster = no_poster
        self._injection = self._config['injection']['enabled']
        if self._injection:
            self.injector = Injector(self._config)

    @property
    def injection(self) -> bool:
        return self._injection

    @property
    def torrent_creation_path(self) -> Path:
        return self._torrent_creation_path

    @property
    def info_path(self) -> Path:
        return self._info_path

    @property
    def is_single_file(self) -> bool:
        return True if self.torrent_creation_path == self.info_path else False

    @property
    def tmdb_id(self) -> int:
        return self._tmdb_id

    @tmdb_id.setter
    def tmdb_id(self, id: int) -> None:
        if isinstance(id, int) and id > 0:
            self._tmdb_id = id
        else:
            raise ValueError('TMDB ID must be a number and greater than zero')

    @property
    def interactive(self) -> bool:
        return self._interactive

    @property
    def upload_type(self) -> str:
        if self._upload_type is not None:
            return self._upload_type
        else:
            raise UnboundLocalError(
                'Cannot access upload type before it is determined')

    @upload_type.setter
    def upload_type(self, type_string: str) -> None:
        if type_string.lower() in self._valid_types:
            self._upload_type = type_string.lower()

    @property
    def is_complete_series(self) -> bool:
        try:
            if self._upload_type and self._tmdb and self._upload_type == 'tv' and len(self._seasons) > 0:
                if self._seasons == [-1]:
                    return True
                if len(self._seasons) == 1 and self._tmdb['number_of_seasons'] == 1:
                    return True
                if len(range(self._seasons[0], self._seasons[1] + 1)) == self._tmdb['number_of_seasons']:
                    return True
        except:
            return False
        return False

    @property
    def tmdb_seasons(self) -> int:
        if self._upload_type and self._tmdb and self._upload_type == 'tv':
            return self._tmdb['number_of_seasons']
        else:
            return 0

    @property
    def screenshot_urls(self) -> List[str]:
        return self._screenshot_urls

    @property
    def created_torrent_path(self) -> Path:
        return self._created_torrent_path

    @property
    def crt_title(self) -> str:
        return self._crt_title

    @property
    def tags(self) -> List[str]:
        return self._tags

    @property
    def uploaded_poster_url(self) -> str:
        return self._uploaded_poster_url

    @property
    def indexer_urls(self) -> List[str]:
        if self._tmdb:
            urls = [self._tmdb['imdb_url'], self._tmdb['tmdb_url']]
            if self._tmdb['tvdb_url']:
                urls.append(self._tmdb['tvdb_url'])
            return urls
        else:
            return ['']

    @property
    def tmdb_description(self) -> str:
        return self._tmdb_description

    @property
    def mediainfo_string(self) -> str:
        return self._mediainfo_string

    @property
    def presentation_dir(self) -> Path:
        return self._presentation_dir

    @property
    def note_text(self) -> Union[str, None]:
        return self._note_text

    @property
    def compression(self) -> bool:
        return self._compression

    @property
    def needs_deint(self) -> bool:
        return self._needs_deint

    def get_mediainfo(self) -> None:
        """Creates a VideoInfo instance and stores the information for later
        """
        mediainfo = VideoInfo(self.info_path, self._temp_dir)
        self._mediainfo_tagging = mediainfo.tagging_info
        self._mediainfo_string = mediainfo.full_str

    def do_screenshots(self) -> None:
        """Determines length of content in ms and calls to take screenshots and upload them
        """
        duration_ms = self._mediainfo_tagging['general']['length'] * 1000
        self._screenshot_urls = asyncio.get_event_loop().run_until_complete(
            images.take_screens_and_upload(
                self._info_path,
                self._temp_dir,
                duration_ms,
                self._image_host,
                self._compression,
                self._imgbb_key,
                self.needs_deint,
                self._tonemap_command,
                self._freeimage_key,
                self._ptpimg_key
            )
        )

    def take_guess(self) -> None:
        """Attempts to correctly guess some parameters based on file/folder names.
            In interactive mode the user is prompted to confirm these are correct.
        """
        guess = guessit(str(self._info_path))
        if self.interactive:
            valid = False
            while not valid:
                print('Guessed parameters from filename:')
                for key, val in enumerate(guess):
                    print(f'  {val:<20}: {guess[val]}')
                uin = input(
                    'If this seems correct, press <enter>.\nOtherwise type a new string to parse: ')
                if uin.strip().lower() == '':
                    valid = True
                else:
                    guess = guessit(uin)
        self._guessed = guess
        if self._torrent_creation_path != self._info_path and self._guessed['type'] == 'episode' or fileutils.number_relevant_files(self._torrent_creation_path) > 1:
            self.upload_type = 'tv'
        else:
            self.upload_type = 'movie'
        if self._mediainfo_tagging['general']['tmdb']:
            try:
                cat, tmdb_id = self._mediainfo_tagging['general']['tmdb'].split(
                    '/')
                self.upload_type = cat
                self.tmdb_id = int(tmdb_id)
            except:
                pass
        if self.interactive:
            valid = False
            while not valid:
                uin = input(
                    f'Determined type of {self.torrent_creation_path.name} is {self.upload_type}.\n Enter new value or continue (<enter>,tv,movie): ').strip().lower()
                if uin == '':
                    valid = True
                elif uin in self._valid_types:
                    self._upload_type = uin
                    valid = True
        if self.upload_type == 'tv':
            seasons = fileutils.parse_seasons_from_folder(
                self.torrent_creation_path)
            seasons = sorted([x if x != 0 else 1 for x in seasons])
            if self.interactive:
                valid = False
                while not valid:
                    uin = input(
                        f'Detected seasons in range: {seasons}.\nContinue or enter correction (<enter>, start-end, single) e.g. 1-5 or 2: ').strip().lower()
                    if uin == '':
                        valid = True
                    else:
                        try:
                            if '-' in uin:
                                split = uin.split('-')
                                seasons = [int(split[0]), int(split[1])]
                                valid = True
                            else:
                                seasons = [int(uin)]
                                valid = True
                        except:
                            continue
            self._seasons = seasons
        if 'year' in self._guessed and isinstance(self._guessed['year'], int):
            self._guessed_year = self._guessed['year']
        if 'edition' in self._guessed:
            self._edition = self._guessed['edition']
        if 'release_group' in self._guessed:
            self._release_group = self._guessed['release_group']

    def get_tmdb_info(self) -> None:
        """Creates an instance of TMDB and attempts to do a lookup. In interactive mode,
            user is prompted to confirm the returns are correct.

        Raises:
            ValueError: If user attempts to give bad input
            ValueError: If user attempts to give bad input
        """
        tmdb = TMDB(self.upload_type, self._tmdb_key, self.tmdb_id,
                    self._guessed['title'], self._guessed_year, self._seasons, self._language)
        valid = False
        while not valid:
            try:
                tmdb.do_initial_search()
            except LookupError as err:
                print(err)
                uin = input(
                    'Please enter a matching TMDB ID or exit to give up: ').strip().lower()
                if uin == 'exit':
                    sys.exit(0)
                else:
                    try:
                        new_id = int(uin)
                        if new_id < 0:
                            raise ValueError
                        tmdb.tmdb_id = new_id
                        tmdb.get_by_id()
                        valid = True
                    except:
                        continue
            finally:
                valid = True
        if self.interactive:
            valid = False
            while not valid:
                info = tmdb.get_info()
                print(
                    f'Info from TMDB:\n'
                    f'  Title        : {info["title"]}\n'
                    f'  ID           : {info["tmdb_id"]}\n'
                    f'  Type         : {info["category"]}'
                )
                if self._upload_type == 'movie':
                    print(
                        f'  Year         : {info["year"]}\n'
                        f'  Runtime      : {info["runtime"]}'
                    )
                elif self._upload_type == 'tv':
                    print(f'  No. Seasons  : {info["number_of_seasons"]}')
                uin = input(
                    'Is this correct? <enter> to confirm or provide new TMDB ID: ').strip().lower()
                if uin == '':
                    valid = True
                    self._tmdb = info
                else:
                    try:
                        new_id = int(uin)
                        if new_id < 0:
                            raise ValueError
                        tmdb.tmdb_id = new_id
                        tmdb.get_by_id()
                    except:
                        continue
        else:
            self._tmdb = tmdb.get_info()
        self._tmdb_description = self._tmdb['description']

    def fetch_poster(self) -> None:
        """Runs the function to grab a poster and re-upload it
        """
        if not self._no_poster:
            self._uploaded_poster_url = asyncio.get_event_loop().run_until_complete(images.download_and_upload_poster(
                self._tmdb['poster_url'], self._temp_dir, self._image_host, self._imgbb_key, self._freeimage_key, self._ptpimg_key))
        else:
            self._uploaded_poster_url = 'None: you must upload one yourself and link it here.'

    def create_description(self) -> None:
        """Calls text functions to create description from previously determined metadata and
            write it to a file
        """
        self._crt_title = textutils.make_title(self._tmdb['title'], self._tmdb['original_title'], self._tmdb['alternate_title'],
                                               self._tmdb['year'], self._edition, self._tmdb['original_language'], self._seasons, self.is_complete_series, self.upload_type, self.tmdb_seasons)
        self._tags = textutils.make_tag_list(
            self._tmdb['genres'], self._mediainfo_tagging, self._tmdb['year'], self._tmdb['original_language'], self._release_group)
        fileutils.check_individual_presentation_folder(self._presentation_dir)
        self._presentation = presentation.create_presentation(self)
        fileutils.write_presentation(
            self._presentation_dir, self._presentation, self.torrent_creation_path)

    def create_torrent(self) -> None:
        """Calls to create the torrent, with a hashing progress bar
        """
        self._bar = TorrentBar()
        torrent.create_torrent(self.torrent_creation_path,
                               self._presentation_dir, self._crt_key, self._hashing_threads,
                               self._progress_callback, self._ignore_globs)
        self._bar.finish()

    def _progress_callback(self, torrent, filepath, pieces_done, pieces_total) -> None:
        """Callback function for torrent creation to update the progress bar

        Args:
            torrent: Unused
            filepath: Unused
            pieces_done: How many pieces are down so far
            pieces_total: How many pieces total there are
        """
        percent_done = 100 * pieces_done / pieces_total
        if int(percent_done) > int(self._bar.percent):
            self._bar.goto(percent_done)


if __name__ == '__main__':
    pass
