
from shutil import which


def check_depedencies():
    if which("mediainfo") is None:
        print("Could not find mediainfo on your path, is it installed?")
        exit(1)
    if which("ffmpeg") is None:
        print("Could not find ffmpeg on your path, is it installed?")
        exit(1)
    if which("ffprobe") is None:
        print("Could not find ffprobe on your path, is it installed?")
        exit(1)
