from pathlib import Path
from typing import List
import shutil
import re

import yaml
from guessit import guessit

from src.utils.formats import VALID_EXTENSIONS, TONEMAPPING_ALGORITHMS


def parse_file_args(files: List[str]) -> List[dict]:
    """Parses a list of user-provided paths to determine if they exist, and finds a
        valid file for metadata if it is a folder

    Args:
        files (List[str]): List of paths to process

    Raises:
        FileNotFoundError: If a valid media file could not be located
        FileNotFoundError: If a valid media file could not be located

    Returns:
        List[dict]: Each input item is returned with a path for torrent creation and a
            path for metadata creation
    """
    paths = [Path(x) for x in files]
    tested_paths = []
    for path in paths:
        if path.exists() and path.is_file():
            tested_paths.append({
                'torrent_path': path,
                'info_path': path
            })
        elif path.exists() and path.is_dir():
            found = next((x for x in sorted(path.iterdir(), key=lambda s: s.name)
                         if x.is_file() and x.suffix.lower() in VALID_EXTENSIONS), None)
            if found is not None:
                file = Path(found)
            else:
                subfolder = next(
                    (x for x in sorted(path.iterdir(), key=lambda s: s.name) if x.is_dir()), None)
                if subfolder is not None:
                    found_deeper = next((x for x in sorted(subfolder.iterdir(), key=lambda s: s.name) if x.is_file(
                    ) and x.suffix.lower() in VALID_EXTENSIONS), None)
                    if found_deeper is not None:
                        file = Path(found_deeper)
                    else:
                        raise FileNotFoundError(
                            f'Could not find a valid file in given directory {path}')
                else:
                    raise FileNotFoundError(
                        f'Could not find a valid file in given directory {path}')

            tested_paths.append({
                'torrent_path': path,
                'info_path': file
            })

    return tested_paths


def check_temp_folder(temp_dir: Path) -> None:
    """Checks if temporary folder exists and is safe to use. Prompts user to delete folder
        if it contains things already, or creates it if it does note exist

    Args:
        temp_dir (Path): Temporary directory to check

    Raises:
        FileExistsError: If the path exists and is not a directory
    """
    if temp_dir.is_dir():
        if any(Path(temp_dir).iterdir()):
            shutil.rmtree(temp_dir)
            temp_dir.mkdir()
    elif temp_dir.exists():
        raise FileExistsError(
            'The temporary path exists but is not directory, please correct and try again')
    else:
        temp_dir.mkdir()


def check_presentations_folder(presentation_folder: Path) -> None:
    """Checks if presentation folder exists and creates it if necessary

    Args:
        presentation_folder (Path): Parent folder to hold presentation data

    Raises:
        FileExistsError: If the path already exists but is not a directory
    """
    if presentation_folder.exists():
        if not presentation_folder.is_dir():
            raise FileExistsError(
                'Presentation path exists but is not a directory, please correct and try again')
    else:
        presentation_folder.mkdir()


def parse_config(config_file: Path) -> dict:
    """Loads the yaml config file

    Args:
        config_file (Path): Path to the config file

    Returns:
        dict: Parameters that were safely loaded from the file
    """
    if config_file.is_file():
        with config_file.open('r', encoding='utf8') as fo:
            config = yaml.safe_load(fo)
        config['dirs']['temp'] = Path(config['dirs']['temp'])
        config['dirs']['presentations'] = Path(config['dirs']['presentations'])
        if config['options']['tonemap_algorithm'] not in TONEMAPPING_ALGORITHMS:
            valid_algorithms = '\n\t'.join(TONEMAPPING_ALGORITHMS.keys())
            raise ValueError(f'tonemap_algorithm must be one of these:\n\t{valid_algorithms}')
        else:
            config['options']['tonemap_command'] = TONEMAPPING_ALGORITHMS[config['options']['tonemap_algorithm']]
        return config
    else:
        raise FileNotFoundError('Cannot find config file: {config_file}')


def parse_seasons_from_folder(root_path: Path) -> List[int]:
    """Attempts guess TV seasons present in folder

    Args:
        root_path (Path): The root directory specified by user

    Returns:
        List[int]: Returns [] if not seasons found, [single] if one found and [first, last]
            if more were found
    """
    if re.match('s[0-9]{2,5}', root_path.name.lower()) or re.match('Season.[0-9]', root_path.name.lower()):
        temp_guessed = guessit(str(root_path.name))
        temp_season = temp_guessed['season'] if 'season' in temp_guessed else None
        if isinstance(temp_season, int):
            return [temp_guessed]
        else:
            return [-1]
    guessed_seasons = []
    for dir in sorted(root_path.glob('**'), key=lambda s: s.name):
        if dir.is_dir():
            temp_guessed = guessit(dir.name)
            temp_season = temp_guessed['season'] if 'season' in temp_guessed else None
            if isinstance(temp_season, int):
                guessed_seasons.append(temp_season)
    if len(guessed_seasons) == 0:
        return [-1]
    elif len(guessed_seasons) == 1 or len(guessed_seasons) == 2:
        return guessed_seasons
    else:
        return [guessed_seasons[0], guessed_seasons[-1]]


def number_relevant_files(lookup_path: Path) -> int:
    """Recursively counts the number of files with a valid file extensions in a directory

    Args:
        lookup_path (Path): Path to rglob through

    Returns:
        int: Number of files found with valid extensions
    """
    file_list = lookup_path.rglob('*')
    count = 0
    for item in file_list:
        if item.suffix in VALID_EXTENSIONS:
            count += 1
    return count


def check_individual_presentation_folder(out_dir: Path) -> None:
    """Removes an individual presentation if it exists, and creates it either way

    Args:
        out_dir (Path): Path to individual presentation directory
    """
    if out_dir.exists():
        shutil.rmtree(out_dir)
    out_dir.mkdir()


def write_presentation(out_path: Path, data: str, torrent_name: Path) -> None:
    """Writes out the given text presentation to a file

    Args:
        out_path (Path): Path to the individual presentation folder
        data (str): Description text
        torrent_name (Path): Used to create a *.txt filename
    """
    out_file = out_path / f'{torrent_name.name}.txt'
    with out_file.open('w', encoding='utf8') as fo:
        fo.write(data)


if __name__ == '__main__':
    pass
