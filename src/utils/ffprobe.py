
import subprocess
from pathlib import Path
import json

from src.utils.formats import HDR_TRANSFERS, INTERLACED_ORDERS


def check_needs_tonemap(input_file: Path) -> bool:
    cmd = [
        'ffprobe',
        '-v',
        'error',
        '-show_streams',
        '-select_streams',
        'v:0',
        '-of',
        'json',
        '-i',
        str(input_file)
    ]
    process = subprocess.run(cmd, capture_output=True, check=True)
    js = json.loads(process.stdout)
    colour_transfer = js['streams'][0].get('color_transfer', None)
    if colour_transfer in HDR_TRANSFERS:
        return True
    return False


def check_needs_deint(input_file: Path) -> bool:
    cmd = [
        'ffprobe',
        '-v',
        'error',
        '-show_streams',
        '-select_streams',
        'v:0',
        '-of',
        'json',
        '-i',
        str(input_file)
    ]
    process = subprocess.run(cmd, capture_output=True, check=True)
    js = json.loads(process.stdout)
    interlaced = js['streams'][0].get('field_order', None)
    if interlaced in INTERLACED_ORDERS:
        return True
    return False
